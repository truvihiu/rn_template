import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import {TailwindProvider} from 'tailwind-rn';

import RootNavigation from './RootNavigation';
import store from './redux/store';
import utilities from './tailwind.json';

const App = () => {
  return (
    <Provider store={store}>
      <TailwindProvider utilities={utilities}>
        <NavigationContainer>
          <RootNavigation />
        </NavigationContainer>
      </TailwindProvider>
    </Provider>
  );
};

export default App;
