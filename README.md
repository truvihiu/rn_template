## Installation

Use the package manager [yarn](https://yarnpkg.com/) or [npm]() to install.

```bash
yarn install
```

## Run project
Open 1st terminal
```
yarn start
```

Open 2nd terminal (run on android or ios device)
```
yarn android
```

Open 3rd terminal to run tailwind compiler
```
yarn dev:tailwind
```
