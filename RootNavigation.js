import React from 'react';
import {View, Text} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Home from './screens/Home';
import PostList from './screens/PostList';
import PostAdd from './screens/PostAdd';
import {
  HOME_ROUTE,
  LIST_POST_ROUTE,
  POST_ADD_ROUTE,
  POST_EDIT_ROUTE,
} from './shared/constants/common';
import PostEdit from './screens/PostEdit';

const Stack = createNativeStackNavigator();

function RootNavigation() {
  return (
    <Stack.Navigator>
      <Stack.Screen name={HOME_ROUTE} component={Home} />
      <Stack.Screen name={LIST_POST_ROUTE} component={PostList} />
      <Stack.Screen name={POST_ADD_ROUTE} component={PostAdd} />
      <Stack.Screen name={POST_EDIT_ROUTE} component={PostEdit} />
    </Stack.Navigator>
  );
}

export default RootNavigation;
