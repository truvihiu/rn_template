export const HOME_ROUTE = 'Home';
export const LIST_POST_ROUTE = 'ListPost';
export const POST_ADD_ROUTE = 'PostAdd';
export const POST_EDIT_ROUTE = 'PostEdit';

// AsyncStorage
export const AUTH_STORAGE = '@auth';

export const GENERATE_STATUS = (name = '') => ({
  IDLE: `idle-${name}`,
  LOADING: `loading-${name}`,
  SUCCESS: `success-${name}`,
  FAILED: `failed-${name}`,
});
