export const URL_LIST_POST = '/posts';
export const URL_DELETE_POST = postId => `/posts/${postId}`;
export const URL_UPDATE_POST = postId => `/posts/${postId}`;
export const URL_READ_POST = postId => `/posts/${postId}`;
export const URL_CREATE_POST = '/posts';
