import React from 'react';
import {View, Text, TouchableOpacity, ActivityIndicator} from 'react-native';
import {useTailwind} from 'tailwind-rn';

const CustomButton = ({loading, disabled, label, children, ...props}) => {
  const tw = useTailwind();
  return (
    <TouchableOpacity
      {...props}
      disabled={loading || disabled}
      style={tw('flex-row items-center bg-blue-400 px-4 py-2 rounded-lg')}>
      {loading && <ActivityIndicator color="white" style={tw('mr-2')} />}
      {label ? (
        <Text style={tw('text-white text-xl font-semibold')}>{label}</Text>
      ) : (
        children
      )}
    </TouchableOpacity>
  );
};

export default CustomButton;
