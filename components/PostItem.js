import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {useTailwind} from 'tailwind-rn';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import CustomButton from './CustomButton';

const PostItem = ({post, onEdit, onDelete}) => {
  const tw = useTailwind();
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{post.title}</Text>
      <Text style={styles.body}>{post.body}</Text>
      <View style={tw('flex flex-row items-center justify-end m-3')}>
        <TouchableOpacity onPress={onEdit}>
          <FontAwesome5 name="edit" size={24} color="white" />
        </TouchableOpacity>
        <View style={tw('m-2')} />
        <TouchableOpacity onPress={onDelete}>
          <FontAwesome5 name="trash" size={24} color="white" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'orange',
    marginHorizontal: 10,
    marginVertical: 15,
  },
  title: {
    padding: 10,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
  body: {
    padding: 10,
    fontWeight: 'bold',
    fontSize: 15,
  },
});

export default PostItem;
