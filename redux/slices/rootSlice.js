import {combineReducers} from '@reduxjs/toolkit';

import posts from './postsSlice';
import postDetail from './postDetailSlice';

export const rootReducer = combineReducers({posts, postDetail});
