import {createSlice} from '@reduxjs/toolkit';
import {GENERATE_STATUS} from '../../shared/constants/common';

export const LIST_POST_STATUS = GENERATE_STATUS('list-post');
export const DELETE_POST_STATUS = GENERATE_STATUS('delete-post');

export const postsSlice = createSlice({
  name: 'posts',
  initialState: {
    list: [],
    error: null,
    status: 'idle',
  },
  reducers: {
    listRequest: state => {
      state.status = LIST_POST_STATUS.LOADING;
      state.error = null;
    },
    listSuccess: (state, {payload: {data}}) => {
      state.status = LIST_POST_STATUS.SUCCESS;
      state.list = data || [];
    },
    listFailed: (state, {payload: {error}}) => {
      state.status = LIST_POST_STATUS.FAILED;
      state.error = error || 'Failed to list post';
    },
    deleteRequest: state => {
      state.status = DELETE_POST_STATUS.LOADING;
      state.error = null;
    },
    deleteSuccess: (state, {payload: {postId}}) => {
      state.status = DELETE_POST_STATUS.SUCCESS;
      const newList = state.list.filter(item => item.id != postId);
      state.list = newList;
    },
    deleteFailed: (state, {payload: {error}}) => {
      state.status = DELETE_POST_STATUS.FAILED;
      state.error = error || 'Failed to delete post';
    },
  },
});

// Action creators are generated for each case reducer function
export const postsActions = postsSlice.actions;
export const postsSelectors = {
  selectEntire: state => state.posts,
};

export default postsSlice.reducer;
