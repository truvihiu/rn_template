import {createSlice} from '@reduxjs/toolkit';
import {GENERATE_STATUS} from '../../shared/constants/common';

export const CREATE_POST_STATUS = GENERATE_STATUS('create-post');
export const UPDATE_POST_STATUS = GENERATE_STATUS('update-post');
export const READ_POST_STATUS = GENERATE_STATUS('read-post');

export const postDetailSlice = createSlice({
  name: 'postDetail',
  initialState: {
    item: {},
    error: null,
    status: 'idle',
  },
  reducers: {
    createRequest: state => {
      state.status = CREATE_POST_STATUS.LOADING;
      state.error = null;
    },
    createSuccess: (state, {payload: {data}}) => {
      state.status = CREATE_POST_STATUS.SUCCESS;
      state.item = data || {};
    },
    createFailed: (state, {payload: {error}}) => {
      state.status = CREATE_POST_STATUS.FAILED;
      state.error = error || 'Failed to create post';
    },
    readRequest: state => {
      state.status = READ_POST_STATUS.LOADING;
      state.error = null;
    },
    readSuccess: (state, {payload: {data}}) => {
      state.status = READ_POST_STATUS.SUCCESS;
      state.item = data || {};
    },
    readFailed: (state, {payload: {error}}) => {
      state.status = READ_POST_STATUS.FAILED;
      state.error = error || 'Failed to update post';
    },
    updateRequest: state => {
      state.status = UPDATE_POST_STATUS.LOADING;
      state.error = null;
    },
    updateSuccess: (state, {payload: {data}}) => {
      state.status = UPDATE_POST_STATUS.SUCCESS;
      state.item = data || {};
    },
    updateFailed: (state, {payload: {error}}) => {
      state.status = UPDATE_POST_STATUS.FAILED;
      state.error = error || 'Failed to update post';
    },
  },
});

// Action creators are generated for each case reducer function
export const postDetailActions = postDetailSlice.actions;
export const postDetailSelectors = {
  selectEntire: state => state.postDetail,
};

export default postDetailSlice.reducer;
