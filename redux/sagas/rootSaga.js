import {all} from 'redux-saga/effects';

import postsSaga from './postsSaga';
import postDetailSaga from './postDetailSaga';

export default function* rootSaga() {
  yield all([postsSaga(), postDetailSaga()]);
}
