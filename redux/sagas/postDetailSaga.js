import {all, put, takeEvery, call, delay} from 'redux-saga/effects';

import Api from '../../shared/configs/api';
import {
  URL_CREATE_POST,
  URL_READ_POST,
  URL_UPDATE_POST,
} from '../../shared/constants/endpoints';
import {postDetailActions} from '../slices/postDetailSlice';

function* createWorker({payload: {form}}) {
  try {
    const {data} = yield call(() => Api.post(URL_CREATE_POST, form));
    yield delay(500); // slow down api response
    yield put(postDetailActions.createSuccess({data}));
  } catch (error) {
    yield put(postDetailActions.createFailed({error: error.response}));
  }
}

function* readWorker({payload: {postId}}) {
  try {
    const {data} = yield call(() => Api.get(URL_READ_POST(postId)));
    yield delay(500); // slow down api response
    yield put(postDetailActions.readSuccess({data}));
  } catch (error) {
    yield put(postDetailActions.readFailed({error: error.response}));
  }
}

function* updateWorker({payload: {form}}) {
  try {
    const {data} = yield call(() => Api.put(URL_UPDATE_POST(form.id), form));
    yield delay(500); // slow down api response
    yield put(postDetailActions.updateSuccess({data}));
  } catch (error) {
    yield put(postDetailActions.updateFailed({error: error.response}));
  }
}

export default function* postDetailSaga() {
  yield all([
    yield takeEvery(postDetailActions.createRequest.type, createWorker),
    yield takeEvery(postDetailActions.updateRequest.type, updateWorker),
    yield takeEvery(postDetailActions.readRequest.type, readWorker),
  ]);
}
