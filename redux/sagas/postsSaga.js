import {all, put, takeEvery, call, delay} from 'redux-saga/effects';

import Api from '../../shared/configs/api';
import {URL_LIST_POST, URL_DELETE_POST} from '../../shared/constants/endpoints';
import {postsActions} from '../slices/postsSlice';

function* listWorker({payload}) {
  try {
    const {data} = yield call(() => Api.get(URL_LIST_POST));
    yield delay(500); // slow down api response
    yield put(postsActions.listSuccess({data}));
  } catch (error) {
    yield put(postsActions.listFailed({error: error.response}));
  }
}

function* deleteWorker({payload: {postId}}) {
  try {
    const {data} = yield call(() => Api.delete(URL_DELETE_POST(postId)));
    yield delay(500); // slow down api response
    yield put(postsActions.deleteSuccess({postId}));
  } catch (error) {
    yield put(postsActions.deleteFailed({error: error.response}));
  }
}

export default function* postsSaga() {
  yield all([
    yield takeEvery(postsActions.listRequest.type, listWorker),
    yield takeEvery(postsActions.deleteRequest.type, deleteWorker),
  ]);
}
