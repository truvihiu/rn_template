import React, {useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  Button,
  SafeAreaView,
  FlatList,
  RefreshControl,
} from 'react-native';
import {useTailwind} from 'tailwind-rn';
import {useDispatch, useSelector} from 'react-redux';

import {
  LIST_POST_STATUS,
  postsActions,
  postsSelectors,
} from '../redux/slices/postsSlice';
import {POST_ADD_ROUTE, POST_EDIT_ROUTE} from '../shared/constants/common';
import PostItem from '../components/PostItem';
import PostAdd from './PostAdd';
import {postDetailActions} from '../redux/slices/postDetailSlice';

const STATUS = LIST_POST_STATUS;

const PostList = ({navigation}) => {
  const tw = useTailwind();
  const dispatch = useDispatch();
  const {list, status} = useSelector(postsSelectors.selectEntire);

  const fetchPosts = () => {
    dispatch(postsActions.listRequest());
  };

  useEffect(() => {
    fetchPosts();
  }, []);

  return (
    <SafeAreaView style={tw('flex-1 justify-center items-center')}>
      <View style={tw('m-4')}>
        <Text style={styles.text}>Post List</Text>
        <Button
          title={'Create a new post'}
          onPress={() => navigation.navigate(POST_ADD_ROUTE)}
        />
      </View>
      <View style={tw('flex-1 justify-center items-center')}>
        {status === STATUS.LOADING ? (
          <ActivityIndicator />
        ) : (
          <FlatList
            refreshControl={
              <RefreshControl
                colors={['#9Bd35A', '#689F38']}
                refreshing={status === STATUS.LOADING}
                onRefresh={fetchPosts}
              />
            }
            data={list}
            renderItem={({item}) => (
              <PostItem
                post={item}
                onDelete={() =>
                  dispatch(postsActions.deleteRequest({postId: item.id}))
                }
                onEdit={() =>
                  navigation.navigate(POST_EDIT_ROUTE, {postId: item.id})
                }
              />
            )}
            keyExtractor={item => item.id}
          />
        )}
      </View>
    </SafeAreaView>
  );
};

export default PostList;

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Hurricane-Regular',
    fontSize: 60,
    color: 'red',
  },
});
