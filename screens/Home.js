import React from 'react';
import {SafeAreaView, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {useTailwind} from 'tailwind-rn';

import Icon from 'react-native-vector-icons/FontAwesome';
import {LIST_POST_ROUTE} from '../shared/constants/common';

const Home = ({navigation}) => {
  const tw = useTailwind();

  return (
    <SafeAreaView style={tw('flex-1 justify-center items-center')}>
      <Text style={styles.text}>Home</Text>
      <TouchableOpacity onPress={() => navigation.navigate(LIST_POST_ROUTE)}>
        <Icon name="rocket" size={50} color={'black'} />
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Hurricane-Regular',
    fontSize: 60,
    color: 'red',
    padding: 10,
  },
});
