import React from 'react';
import {View, SafeAreaView, StyleSheet, Text, Button} from 'react-native';
import {useTailwind} from 'tailwind-rn';
import {yupResolver} from '@hookform/resolvers/yup';
import {useForm} from 'react-hook-form';
import * as yup from 'yup';

import InputControl from '../components/InputControl';
import {
  CREATE_POST_STATUS,
  postDetailActions,
  postDetailSelectors,
} from '../redux/slices/postDetailSlice';
import {useDispatch, useSelector} from 'react-redux';
import CustomButton from '../components/CustomButton';

const STATUS = CREATE_POST_STATUS;

const PostAdd = () => {
  const tw = useTailwind();
  const dispatch = useDispatch();
  const {status, item} = useSelector(postDetailSelectors.selectEntire);

  const schema = yup.object().shape({
    title: yup.string().required(),
    body: yup.string().required(),
  });

  const {handleSubmit, control} = useForm({
    defaultValues: {userId: 1, title: '', body: ''},
    resolver: yupResolver(schema),
  });

  const onSubmit = form => {
    dispatch(postDetailActions.createRequest({form}));
  };
  const onError = error => {
    console.log({error});
  };
  return (
    <SafeAreaView style={tw('flex-1 items-center justify-center')}>
      <Text style={styles.text}>Create Post</Text>
      <InputControl label="Title" control={control} name="title" />
      <View style={tw('m-2')} />
      <InputControl label="Body" control={control} name="body" />

      <CustomButton
        label="Create"
        loading={status === STATUS.LOADING}
        onPress={handleSubmit(onSubmit, onError)}
      />

      <Text>{JSON.stringify(item, null, 2)}</Text>
    </SafeAreaView>
  );
};

export default PostAdd;

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Hurricane-Regular',
    fontSize: 60,
    color: 'red',
  },
});
